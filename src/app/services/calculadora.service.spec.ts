import { TestBed } from '@angular/core/testing';
import { CalculadoraService } from './calculadora.service';


describe('A classe Calculadora', () => {
  let calculadora: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    calculadora = TestBed.inject(CalculadoraService);
  });

  // Funcionalidade
  describe('deve realizar divisões', () => {

    // caso de teste para a funcionalidade
    it('entre números inteiros ', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });

    it('com exceção do zero ', () => {
      const result = calculadora.divide(8, 0);
      expect(typeof result).toEqual('string');
    });
  });

  describe('deve realizar soma', () => {

    it('entre números inteiros', () => {
      const result = calculadora.soma(16, 16);
      expect(result).toBe(32);
    });

    it('entre números decimais', () => {
      const result = calculadora.soma(1.5, 1.5);
      expect(result).toBe(3);
    });
  });

  describe('deve realizar subtração', () => {
    it('entre números inteiros', () => {
      const result = calculadora.subtrai(12, 6);
      expect(result).toBe(6);
    });

    it('entre números negativos', () => {
      const result = calculadora.subtrai(-4, -2);
      expect(result).toBe(-2);
    });
  });
  
  describe('deve realizar multiplicação', () => {
    it('entre números inteiros', () => {
      const result = calculadora.multiplica(18, 9);
      expect(result).toBe(162);
    });
  });
});
