import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(algarismo1, algarismo2){
    if(algarismo2 === 0){
      return 'Não existe divisão por 0'
    }
    return algarismo1 / algarismo2;
  }

  soma(algarismo1, algarismo2){
    return algarismo1 + algarismo2;
  }

  subtrai(algarismo1, algarismo2){
    return algarismo1 - algarismo2;
  }

  multiplica(algarismo1, algarismo2){
    return algarismo1 * algarismo2;
  }
}
