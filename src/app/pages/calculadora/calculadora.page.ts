import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
    titulo = 'Calculadora';
    form: FormGroup;
    quociente: string | number;

    constructor(
      private builder: FormBuilder,
      private calculadora: CalculadoraService
    ) { }

    ngOnInit() {
      this.form = this.builder.group({
        algarismo1: ['', [Validators.required]],
        algarismo2: ['', [Validators.required]]
      });
    }

    dividir(){
      const data = this.form.value;      
      const algarismo1 = data.algarismo1;
      const algarismo2 = data.algarismo2;

      this.quociente = this.calculadora.divide(algarismo1, algarismo2);
    }

    somar(){
      const data = this.form.value;      
      const algarismo1 = data.algarismo1;
      const algarismo2 = data.algarismo2;

      this.quociente = this.calculadora.soma(algarismo1, algarismo2);
    }

    subtrair(){
      const data = this.form.value;      
      const algarismo1 = data.algarismo1;
      const algarismo2 = data.algarismo2;

      this.quociente = this.calculadora.subtrai(algarismo1, algarismo2);
    }

    multiplicar(){
      const data = this.form.value;      
      const algarismo1 = data.algarismo1;
      const algarismo2 = data.algarismo2;

      this.quociente = this.calculadora.multiplica(algarismo1, algarismo2);
    }
  
}
