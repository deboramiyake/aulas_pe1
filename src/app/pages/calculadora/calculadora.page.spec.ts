import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { IonicModule } from '@ionic/angular';
import { CalculadoraPage } from './calculadora.page';

describe('CalculadoraPage', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um titulo', () => {
    expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o titulo', () => {
    const result = view.querySelector('.title').textContent;
    expect(result).toEqual('Calculadora');
  });

  it('deve estar com o botão desabilitado', () => {
    expect(model.form.invalid).toBeTrue();
  });

  it('divide a partir de clique no botão', () => {
    //arrange
    model.form.controls.algarismo1.setValue(20);
    model.form.controls.algarismo2.setValue(4);
  
    //act
    const divide = fixture.debugElement.query(By.css('#divide'));
    divide.triggerEventHandler('click', null);
    fixture.detectChanges();

    //assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('5');
  });
});
